/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react";
import PropTypes from "prop-types";
import { useStaticQuery, graphql } from "gatsby";
import Banner from "../components/banner";
import Header from "./header";
import "./layout.css";
import Footer from "./footer";

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `);

  return (
    <div style={{ backgroundColor: "#202A44", minHeight: "100vh" }}>
      {/* <Header siteTitle={data.site.siteMetadata.title} /> */}
      <Banner text={data.site.siteMetadata.title} />
      <div style={{ display: "flex", justifyContent: "center" }}>
        <hr
          style={{
            height: "1px",
            width: "70%",
            backgroundColor: "#aaaaaa",
          }}
        />
      </div>
      <div
        style={{
          margin: `0 auto`,
          paddingTop: 0,
        }}
      >
        <main
          style={{
            display: "flex",
            justifyContent: "center",
            alignContent: "center",
            flexDirection: "column",
          }}
        >
          {children}
        </main>
        <Footer />
      </div>
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
