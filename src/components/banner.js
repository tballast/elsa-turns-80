import { Link } from "gatsby";
import PropTypes from "prop-types";
import React from "react";

const Banner = ({ text }) => (
  <div
    style={{
      paddingTop: `50px`,
      padding: `1.45rem 1.0875rem`,
    }}
  >
    <h1
      style={{
        display: "flex",
        justifyContent: "center",
        margin: 0,
        color: "#fefefe",
        fontFamily: "Lora, serif",
      }}
    >
      {text}
    </h1>
  </div>
);

Banner.propTypes = {
  text: PropTypes.string,
};

Banner.defaultProps = {
  text: ``,
};

export default Banner;
