import React from "react";

const ElsaSlideshow = () => {
  return (
    <div
      style={{
        position: "relative",
        // left: "5%",
        paddingBottom: "45.25%", // set the aspect ratio here as (height / width) * 100%
        // paddingBottom: "56.25%" // set the aspect ratio here as (height / width) * 100%
        height: "0px",
        overflow: "hidden",
        maxWidth: "100%",
      }}
    >
      <iframe
        style={{
          position: "absolute",
          top: "0px",
          left: `0px`,
          width: "100%",
          height: "100%",
        }}
        src="https://www.youtube.com/embed/3UsM-6jmU2s"
        title="Elsa turns 80"
        frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen
      ></iframe>
    </div>
  );
};

export default ElsaSlideshow;
