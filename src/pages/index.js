import React from "react";
import { Link } from "gatsby";
import { useStaticQuery, graphql } from "gatsby";
import useWindowDimensions from "../hooks/use-window-dimensions";

import Layout from "../components/layout";
import Image from "../components/image";
import SEO from "../components/seo";
import WildElsa from "../components/wild-elsa";
import ElsaSlideshow from "../components/elsa-slideshow";

const IndexPage = () => {
  const data = useStaticQuery(graphql`
    query TitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `);

  const { height, width } = useWindowDimensions();
  const pad = (width * 0.1) / 2;

  return (
    <Layout>
      <SEO title="Home" />

      {/* <p>Happy 80th Birthday Elsa!</p> */}
      {/* https://jsfiddle.net/u8nem8xb/ */}
      {/* https://help.issuu.com/hc/en-us/articles/115000631608-Resizing-Embeds-To-Be-Responsive */}
      <div
        style={{
          width: "90%",
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          alignSelf: "center",
        }}
      >
        <WildElsa />
        {/* <p>Happy 80th Elsa!</p> */}
        <div style={{ minHeight: "16px" }} />
        <ElsaSlideshow />
      </div>
    </Layout>
  );
};

export default IndexPage;
